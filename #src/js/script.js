//==============================================================================================================
//WebP
function testWebP(callback) {
  let webP = new Image();
  webP.onload = webP.onerror = function () {
    callback(webP.height === 2);
  };
  webP.src = "data:image/webp;base64,UklGRjoAAABXRUJQVlA4IC4AAACyAgCdASoCAAIALmk0mk0iIiIiIgBoSygABc6WWgAA/veff/0PP8bA//LwYAAA";
}
testWebP(function (support) {
  if (support === true) {
    document.querySelector('html').classList.add('_webp');
  } else {
    document.querySelector('html').classList.add('_no-webp');
  }
});
//==============================================================================================================
//Render images from Pixabay api(for Home images), local storage(for Favourite images)
document.getElementById("findHomeImagesButton").addEventListener('click', renderHomeImages);
document.getElementById("tab-favourite").addEventListener('click', renderFavouriteImages);

function renderHomeImages() {
  let searchedWord = document.getElementById("keyWord").value;
  let requestedUrl = 'https://pixabay.com/api/?key=22590659-4d1c1e1774b87382b97cab781&&q=' + searchedWord + '&image_type=photo';
  let receivedImagesFromApi = [];

  createNewTitleForHomeImages(searchedWord);

  fetch(requestedUrl)
  //defined the status
  .then(function (response) {
    if (response.status !== 200) {
      return Promise.reject(new Error(response.statusText))
    }
    return Promise.resolve(response)
  })
  //response returned in json
  .then(function (response) {
    return response.json()
  })
  //put data into variable 
  .then(function (data) {
    receivedImagesFromApi = data.hits;
    cleanRenderedImages("homeDiv");
    renderImages(receivedImagesFromApi, "homeDiv", "images__label-heart");
  })
  //define error
  .catch(function (error) {
    alert('Something went wrong');
  })
}

function renderFavouriteImages() {
  let favouriteImagesArray = getFavouriteImagesArrayFromLocalStorage();
  let imagesExist = Boolean(Object.keys(favouriteImagesArray).length);

  createTitleForFavouriteImages(imagesExist);
  cleanRenderedImages("favouriteDiv");
  renderImages(favouriteImagesArray, "favouriteDiv", "images__label-cross");
}
//==============================================================================================================
function renderImages(receivedImages, targetDivId, labelIcon) {
  //using "for" for both: [] and {} instead "forEach"(that works only for [])
  for(let key in receivedImages) {
    let labelIconUpdated = labelIcon;
    let image = receivedImages[key];
    if (labelIconUpdated === "images__label-heart" && isImageIdExistInStorage(image.id)){
      labelIconUpdated += " _active";
    }
    let newDiv = document.createElement('div');
    newDiv.className = "images__column";
    newDiv.innerHTML = '<div><div class="images__item"><img class="images__img" src="' + image.webformatURL + '" alt="image"><div class="images__label ' + labelIconUpdated +'" data-image-id = "' + image.id + '"></div></div>';
    let newDivWraper = document.getElementById(targetDivId);
    newDivWraper.append(newDiv);
  }
  if(targetDivId === "favouriteDiv") {
    addOnClickOnCrossLabel();
  }
  else {
    addOnClickOnHeartLabel();
  }
}

function cleanRenderedImages(targetDivId) {
  let requiredElements = document.getElementById(targetDivId).querySelectorAll('.images__column');

  requiredElements.forEach(element => {
    element.remove();
  });
}

function createNewTitleForHomeImages(targetWord) {
  let oldHomeTitle = document.querySelectorAll('.home__title');
  
  oldHomeTitle.forEach(title => {
   title.remove();
  })

  let newHomeTitle = document.createElement('div');
  newHomeTitle.className = "home__title title";
  newHomeTitle.innerHTML = 'Result for "<strong>' + targetWord + '</strong>"';
  let newDivForHomeItems = document.getElementById("homeDiv");
  
  newDivForHomeItems.before(newHomeTitle);
}

function createTitleForFavouriteImages(imagesExist) {
  let favouriteTitle = document.querySelector('.favourite__title');
  if (typeof favouriteTitle !== 'undefined' && favouriteTitle !== null) {
    favouriteTitle.remove();
  }

  favouriteTitle = document.createElement('div');
  favouriteTitle.className = "favourite__title title";
  if (imagesExist){
    favouriteTitle.innerHTML = 'Favourite images';
  }
  else {
    favouriteTitle.innerHTML = 'Please add favourite images';
  }

  let DivForFavouriteItems = document.getElementById("favouriteDiv");
  DivForFavouriteItems.before(favouriteTitle);
}
//==============================================================================================================
function addOnClickOnHeartLabel() {
  let heartLabels = document.querySelectorAll(".images__label-heart");

  heartLabels.forEach(heartLabel => {
    heartLabel.addEventListener("click", function (e) {
      //get id from custom attribute
      let imageId = this.dataset.imageId;
      if (isImageIdExistInStorage(imageId)) {
        deleteFavouriteImageFromLocalStorage(imageId);
        this.classList.remove('_active');
      }
      else {
        //identify favourite image src
        let previousElement = this.previousElementSibling;
        //add previous element src and this element id
        addFavouriteImageToLocalStorage(imageId, previousElement.src);
        this.classList.add('_active');
      }
    });
  }); 
}

function addOnClickOnCrossLabel() {
  let crossLabels = document.querySelectorAll(".images__label-cross");

  crossLabels.forEach(crossLabel => {
    crossLabel.addEventListener("click", function (e) {
      if (confirm("Are you sure want to delete it?")) {
        //get id from custom attribute
        let imageId = this.dataset.imageId;
        deleteFavouriteImageFromLocalStorage(imageId);

        //identify image in html
        let parentElement = this.parentElement;
        parentElement.parentElement.parentElement.remove();

        //get element by image id at home page
        let elementFromHomePage = document.querySelector('[data-image-id="'+ imageId +'"]');
        elementFromHomePage.classList.remove('_active');
      }
    });
  });
}
//==============================================================================================================
function isImageIdExistInStorage(id) {
  let favouriteImages = getFavouriteImagesArrayFromLocalStorage();
  //if no images in the local storage
  if (typeof favouriteImages !== 'object' || favouriteImages === null) {
    favouriteImages = {};
  }

  return (id in favouriteImages);
}

function deleteFavouriteImageFromLocalStorage(id) {
  let favouriteImages = getFavouriteImagesArrayFromLocalStorage();

  delete favouriteImages[id];

  putFavouriteImagesArrayToLocalStorage(favouriteImages);
}

function addFavouriteImageToLocalStorage(imageId, imageUrl) {
  let favouriteImagesArray = getFavouriteImagesArrayFromLocalStorage();

  //if no images in the local storage
  if (typeof favouriteImagesArray !== 'object' || favouriteImagesArray === null) {
    favouriteImagesArray = {};
  }
  //add new favourite img to array as an object with the new custom index
  favouriteImagesArray[imageId] = {id: imageId, webformatURL: imageUrl};

  putFavouriteImagesArrayToLocalStorage(favouriteImagesArray);
}
//==============================================================================================================
function getFavouriteImagesArrayFromLocalStorage(){
  //receive json from local storage
  let favouriteImagesJson = localStorage.getItem("favouriteImages");
  //json to array

  return JSON.parse(favouriteImagesJson);
}

function putFavouriteImagesArrayToLocalStorage(gottenArray) {
 /* favouriteImagesArray.push({id: imageId, webformatURL: imageUrl});*/
  //array to json
  let favouriteImagesJsonResult = JSON.stringify(gottenArray);
  //json to local storage
  localStorage.setItem("favouriteImages", favouriteImagesJsonResult);
}
//==============================================================================================================
//Tabs
//Show tab content on tabButton Click 
let tabButtons = document.querySelectorAll(".header__button");
let tabContent = document.querySelectorAll(".tab-page");

tabButtons.forEach(tabButton => {
  tabButton.addEventListener("click", function (e) {

    if (!tabButton.classList.contains('_active')){
      tabButtons.forEach(tabButton => {
        tabButton.classList.remove('_active');
      });
     tabContent.forEach(tabBlock => {
        tabBlock.classList.remove('_active');
      });
      this.classList.toggle('_active');

      let activeTabId = this.id;
      let activeTabContentId = activeTabId + "-content";
      let activeTabContent = document.getElementById(activeTabContentId);
      activeTabContent.classList.toggle('_active');
    }
  });
});
//==============================================================================================================







        
        
  